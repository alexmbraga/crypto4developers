package tutorial;

import java.util.Arrays;
import javax.crypto.*;
import java.security.*;
import javax.crypto.spec.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class T07_2_MAC {
  public static void main(String[] args) throws Exception {        
    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    Object[] macs = Security.getAlgorithms("Mac").toArray();
    System.out.println("Provedor: Algoritmo");
    for (int i = 0; i < macs.length;i++) {
      Mac mac = Mac.getInstance(macs[i].toString());
      System.out.println(mac.getProvider()+": "+mac.getAlgorithm());
    }
    
    System.out.println("Macs " + Arrays.toString(macs));
    String msg = "Minha terra tem palmeiras, onde canta o sabiá";
    for (int i = 0; i < macs.length; i++) {
      try{
        KeyGenerator kg = KeyGenerator.getInstance(macs[i].toString());
        SecretKey sk = kg.generateKey();
        Mac mac = Mac.getInstance(macs[i].toString());
        mac.init(sk);
        mac.update(msg.getBytes());
        byte[] tag = mac.doFinal();
        byte[] key2 = sk.getEncoded();
        System.out.println("\nAlgoritmo    : " + mac.getAlgorithm());
        System.out.println("Tamanho (bytes): " + mac.getMacLength());
        System.out.println("Chave          : " + U.b2x(key2));
        System.out.println("mensagem       : " + msg);
        System.out.println("tag1           : " + U.b2x(tag));
        SecretKeySpec ks  = new SecretKeySpec(key2,macs[i].toString());
        Mac mac2 = Mac.getInstance(macs[i].toString());
        mac2.init(ks);
        mac2.update(msg.getBytes());
        byte[] tag2 = mac2.doFinal();
        System.out.println("Tag2:  " + U.b2x(tag2));
        if (Arrays.equals(tag,tag2)) System.out.println("Confere!");
        else                         System.out.println("Não confere!");
      } catch (NoSuchAlgorithmException | InvalidKeyException 
              | IllegalStateException e) { }
    }
  }
}
