package tutorial;

import java.security.Provider;
import java.security.ProviderException;
import java.security.Security;

public class T02_JCA_API_Providers {
    public static void main(String args[]) {
      Provider[] csp = Security.getProviders();
      System.out.println("Provedores de Serviços Criptográfico");
      for (int i =0; i < csp.length; i++) {
      try {
        System.out.printf("%s: %s;\n",csp[i].getName(),csp[i].getInfo());
      } catch (ProviderException ex) {}
    }
    }
}
