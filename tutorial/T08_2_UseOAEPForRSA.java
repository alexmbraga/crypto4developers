package tutorial;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public final class T08_2_UseOAEPForRSA {

  public static void main(String args[]) {
    try {
      Security.addProvider(new BouncyCastleProvider()); // provedor BC
      int ksize = 1024, hsize = 256; // tamanhos da chave e do hash 
      int maxLen = (ksize - 2*hsize)/8 -2;// tamanho máximo do texto claro 
      byte[] pt = U.cancaoDoExilio.substring(0, maxLen).getBytes();// longo
      //byte[] pt = U.cancaoDoExilio.getBytes();// longo
      System.out.println("tamanho (bytes): "+pt.length);
      KeyPairGenerator g = KeyPairGenerator.getInstance("RSA", "BC");
      g.initialize(2048); KeyPair kp = g.generateKeyPair();
      String RSA_OAEP = "RSA/None/OAEPWithSHA256AndMGF1Padding";
      Cipher e = Cipher.getInstance(RSA_OAEP, "BC");
      e.init(Cipher.ENCRYPT_MODE, kp.getPublic());
      Cipher d = Cipher.getInstance(RSA_OAEP, "BC");
      d.init(Cipher.DECRYPT_MODE, kp.getPrivate());

      //System.out.println("Texto claro: " + U.b2x(pt));
      System.out.println("Criptograma: " + U.b2x(e.doFinal(pt)));
      //System.out.println("Criptograma: " + U.b2x(e.doFinal(pt)));
      //System.out.println("Criptograma: " + U.b2x(e.doFinal(pt)));
      System.out.println("Texto claro: " + U.b2x(d.doFinal(e.doFinal(pt))));
    } catch (NoSuchAlgorithmException | NoSuchPaddingException |
            InvalidKeyException | IllegalBlockSizeException |
            BadPaddingException | NoSuchProviderException e) 
    { System.out.println(e); }
  }
}
