package tutorial;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import org.bouncycastle.jce.provider.*;

public class T06_2_EncriptacaoAutenticada {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException,
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    byte[]  k = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
                 0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F};
    byte[] iv = {0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
                 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07};
    
    SecretKeySpec ks = new SecretKeySpec(k, "AES");
    GCMParameterSpec gps = new GCMParameterSpec(128, iv);
    Cipher c = Cipher.getInstance("AES/GCM/NoPadding", "BC");

    // Encriptação pela Ana
    c.init(Cipher.ENCRYPT_MODE, ks, gps);
    byte[] textoclaro = "Criptografia autenticada com AES-GCM.".getBytes();
    byte[] textoAdicional = "Texto adicional em texto claro.".getBytes();
    c.updateAAD(textoAdicional);
    byte[] criptograma = c.doFinal(textoclaro);
    
    // Decriptação pelo Beto
    c.init(Cipher.DECRYPT_MODE, ks, gps); 
    c.updateAAD(textoAdicional);
    boolean ok = true;
    byte[] textoclaroBeto = null;
    try { 
      textoclaroBeto = c.doFinal(criptograma); 
    } catch (AEADBadTagException e) { ok = false; }
    if (ok) {
      System.out.println("Encriptado   com: " + c.getAlgorithm());
      System.out.println("Usando a  tag de: " + gps.getTLen() + " bits");
      System.out.println("Texto claro Beto: " + new String(textoclaroBeto));
    } else { System.out.println("Tag não verificada!");}
  }
}
