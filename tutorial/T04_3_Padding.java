package tutorial;

import javax.crypto.*;
import java.security.*;
import org.bouncycastle.jce.provider.*;

public class T04_3_Padding {
  public static void main(String args[]) {
    try{     
      Security.addProvider(new BouncyCastleProvider()); // provedor BC

      KeyGenerator g = KeyGenerator.getInstance("AES", "BC");
      g.init(128);
      Key k = g.generateKey();
      String paddings[] = {"AES                 ", "AES/ECB/NoPadding   ", 
                           "AES/ECB/PKCS5Padding", "AES/ECB/PKCS7Padding",
                           "AES/ECB/X9.23Padding"};
      for (int i = 0;i < paddings.length; i++) {
        Cipher c = Cipher.getInstance(paddings[i], "BC");
        c.init(Cipher.ENCRYPT_MODE, k);
        byte[] criptograma = c.doFinal("TUTORIALTUTORIAL".getBytes());
        U.println("Padding " + paddings[i] + ": " + U.b2x(criptograma));
      }
    } catch(NoSuchAlgorithmException | NoSuchProviderException 
         |NoSuchPaddingException   | InvalidKeyException 
         | IllegalBlockSizeException | BadPaddingException e) {}
  }
}
