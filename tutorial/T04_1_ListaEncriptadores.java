package tutorial;


import javax.crypto.*;
import java.security.*;
import org.bouncycastle.jce.provider.*;

public class T04_1_ListaEncriptadores {

 public static void main(String args[]) {
    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    Object[] ciphers = Security.getAlgorithms("Cipher").toArray();
    System.out.println("CSP : Algoritmo");
    for (int j=0,i =0; i < ciphers.length; i++) {
      try {
        Cipher c = Cipher.getInstance(ciphers[i].toString());
        System.out.println((++j)+" "+c.getProvider() +": " +c.getAlgorithm());
      } catch (NoSuchAlgorithmException|NoSuchPaddingException
              |ProviderException e){ }
    }
 }
}
