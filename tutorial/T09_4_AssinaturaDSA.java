package tutorial;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class T09_4_AssinaturaDSA {
  public static void main(String[] args) throws Exception {
    Security.addProvider(new BouncyCastleProvider());
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("DSA", "BC");
    kpg = KeyPairGenerator.getInstance("DSA", "BC");
    kpg.initialize(2048, new SecureRandom());
    Signature signerAna = Signature.getInstance("SHA256WithDSA", "BC");
    KeyPair kpAna = kpg.generateKeyPair();

    //Ana assina o documento
    signerAna.initSign(kpAna.getPrivate(), new SecureRandom());
    byte[] documento = U.cancaoDoExilio.getBytes();
    signerAna.update(documento);
    byte[] assinatura = signerAna.sign();

    //Beto verifica a assinatura
    Signature verifierBeto = Signature.getInstance("SHA256WithDSA", "BC");
    verifierBeto.initVerify(kpAna.getPublic());
    verifierBeto.update(documento);
    boolean ok = verifierBeto.verify(assinatura);
    if (ok) { System.out.println("Assinatura verificada!");} 
    else    { System.out.println("Assinatura   inválida!");}

    System.out.println("Chave pública " + kpAna.getPublic());
    System.out.println("Chave privada " + kpAna.getPrivate());
    System.out.println("Assinado com: " + signerAna.getAlgorithm());
    System.out.println("Tamanho da assinatura: "+assinatura.length+" bytes");
    System.out.println("Assinatura  : " + U.b2x(assinatura));
  }
}
