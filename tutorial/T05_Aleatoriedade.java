package tutorial;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Random;
import java.security.SecureRandom;

public class T05_Aleatoriedade {

  public static void main(String[] args) throws NoSuchProviderException {
    try {
      Random r1 = new Random(); 
      Random r2 = new Random();
      SecureRandom sr1 = new SecureRandom();
      SecureRandom sr2 = SecureRandom.getInstance("SHA1PRNG"); //("DRBG");
      SecureRandom sr3 = SecureRandom.getInstance("Windows-PRNG","SunMSCAPI");
      SecureRandom sr4 = SecureRandom.getInstanceStrong();
      byte[] seed = sr1.generateSeed(32);
      r1.setSeed(10); r2.setSeed(10);
      sr1.setSeed(seed); sr2.setSeed(seed);
      sr3.setSeed(seed); sr4.setSeed(seed);
      
      System.out.println(" i,  M1,  M2,  r1,  r2, sr1, sr2, sr3, sr4");
      for (int i = 1; i <= 100; i++){  
        System.out.printf("%2d, %3d, %3d, %3d, %3d, %3d, %3d, %3d, %3d \n",
            i,(int)(Math.random()*100), (int)(Math.random()*100),
            r1.nextInt(100),r2.nextInt(100),
            sr1.nextInt(100),sr2.nextInt(100),
            sr3.nextInt(100),sr4.nextInt(100));
      }
    } catch (NoSuchAlgorithmException e) {System.out.println(e);}
  }
}
