package tutorial;


import java.security.Provider;
import java.security.ProviderException;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class T03_BouncyCastle {
  public static void main(String args[]) {
    try{  
      Provider bc = new BouncyCastleProvider();
      System.out.printf("%s: %s;\n",bc.getName(),bc.getInfo());
      
      Security.addProvider(bc); // provedor BC
      Provider[] csp = Security.getProviders();
      System.out.println("Lista de CSPs");
      for (int i =0; i < csp.length; i++){
       System.out.println(csp[i].getName());
      }
      
      Security.removeProvider("BC");
      Security.insertProviderAt(bc, 1);
      System.out.println("Lista de CSPs");
      csp = Security.getProviders();
      for (int i =0; i < csp.length; i++){
       System.out.println(csp[i].getName());
      }
    } catch (ProviderException e) {System.out.println(e);}
  }
}
