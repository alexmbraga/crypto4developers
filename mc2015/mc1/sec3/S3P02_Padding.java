/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import javax.crypto.*;
import java.security.*;
import org.bouncycastle.jce.provider.*;

// 1.3.2
public class S3P02_Padding {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException,
          InvalidAlgorithmParameterException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC

    // configurações do sistema criptográfico para Ana e Beto
    KeyGenerator g = KeyGenerator.getInstance("AES", "BC"); //gerador de chaves
    g.init(128); //chave AES de 128 bits (compartilhada)
    Key k = g.generateKey(); // Ana cria uma chave pseudoaleatória
    //byte[] iv = U.x2b("1234567890ABCDEF1234567890ABCDEF");
    //AlgorithmParameterSpec aps = new IvParameterSpec(iv);
    Cipher c1 = Cipher.getInstance("AES/ECB/NoPadding", "BC"); //AES sem padding
    Cipher c2 = Cipher.getInstance("AES", "BC"); // modo e padding default
    Cipher c3 = Cipher.getInstance("AES/ECB/PKCS5Padding", "BC"); //PKCS5
    Cipher c4 = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC"); //PKCS7
    Cipher c5 = Cipher.getInstance("AES/ECB/X9.23Padding", "BC"); //X9.23Padding
    c1.init(Cipher.ENCRYPT_MODE, k); // sem IV
    c2.init(Cipher.ENCRYPT_MODE, k); // gerando IV internamente
    c3.init(Cipher.ENCRYPT_MODE, k);
    c4.init(Cipher.ENCRYPT_MODE, k);
    c5.init(Cipher.ENCRYPT_MODE, k);
    //byte[] textoclaroAna = "Testando o AES..".getBytes(); //com 128 bits
    byte[] criptograma1 = c1.doFinal("Testando o AES..".getBytes());
    byte[] criptograma2 = c2.doFinal("Testando o AES..".getBytes());
    byte[] criptograma3 = c3.doFinal("Testando o AES..".getBytes());
    byte[] criptograma4 = c4.doFinal("Testando o AES..".getBytes());
    byte[] criptograma5 = c5.doFinal("Testando o AES..".getBytes());

    U.println("Chave "+U.b2x(k.getEncoded()));
    U.println("texto claro:"+"Testando o AES..");
    U.println("texto claro:"+U.b2x("Testando o AES..".getBytes()));
    U.println("Sem padding:     " + U.b2x(criptograma1));
    U.println("Padding default: " + U.b2x(criptograma2));
    U.println("Com PKCS5:       " + U.b2x(criptograma3));
    U.println("Com PKCS7:       " + U.b2x(criptograma4));
    U.println("Com X9.23Padding:" + U.b2x(criptograma5));
  }
}
