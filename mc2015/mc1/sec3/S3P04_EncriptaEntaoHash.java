/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import org.bouncycastle.jce.provider.*;

//1.3.4. Verificação de integridade e autenticação de mensagem
public class S3P04_EncriptaEntaoHash {

    public static void main(String args[]) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, NoSuchProviderException {

        Security.addProvider(new BouncyCastleProvider()); // provedor BC
        
        // configurações do sistema criptográfico para Ana e Beto
        byte[] k = U.x2b("0123456789ABCDEF0123456789ABCDEF");// 128 bits
        SecretKeySpec sks = new SecretKeySpec(k, "AES");
        Cipher c = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
        MessageDigest md = MessageDigest.getInstance("SHA256", "BC");

        // encriptação pela Ana com hash de integridade
        c.init(Cipher.ENCRYPT_MODE, sks);
        byte[] textoclaroAna = cancaoDoExilio.getBytes(); //com 128 bits
        byte[] criptograma = c.doFinal(textoclaroAna);
        byte[] hash = md.digest(criptograma);

        // decriptação pelo Beto com verificação do hash
        md.reset();
        c.init(Cipher.DECRYPT_MODE, sks); //inicializa o AES para decriptacao
        byte[] textoclaroBeto = c.doFinal(criptograma); // Decriptando
        boolean ok = MessageDigest.isEqual(md.digest(criptograma), hash);
        U.println("Verificado com " + md.getAlgorithm() + ": " + ok);
        U.println("Encriptado com: " + c.getAlgorithm());
        U.println("Valor do  hash: " + U.b2x(hash));
        U.println("Texto claro do Beto:\n" + new String(textoclaroBeto));
    }
    
    //de Antônio Gonçalves Dias - versão sem acentos ...
    // deste modo, cada caracter é um byte. e aconta do getBytes acerta...
    static String cancaoDoExilio = "Minha terra tem palmeiras"
            + "Onde canta o sabiah."
            + "As aves que aqui gorjeiam"
            + "Nao gorjeiam como lah."
            //+ ""
            + "Nosso ceu tem mais estrelas,"
            + "Nossas varzeas tem mais flores."
            + "Nossos bosques tem mais vida,"
            + "Nossa vida mais amores."
            //+ ""
            + "Em cismar, sozinho, aa noite,"
            + "Mais prazer encontro eu lah."
            + "Minha terra tem palmeiras"
            + "Onde canta o sabiah."
            //+ ""
            + "Minha terra tem primores"
            + "Que tais nao encontro eu cah;"
            + "Em cismar — sozinho, aa noite —"
            + "Mais prazer encontro eu lah."
            + "Minha terra tem palmeiras"
            + "Onde canta o sabiah."
            //+ ""
            + "Nao permita Deus que eu morra"
            + "Sem que eu volte para lah;"
            + "Sem que desfrute os primores"
            + "Que nao encontro por cah;"
            + "Sem que ainda aviste as palmeiras"
            + "Onde canta o sabiah.";
}
