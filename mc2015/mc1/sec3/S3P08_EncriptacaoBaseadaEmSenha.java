/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec3;

import javax.crypto.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.jce.provider.*;

// 1.3.6. Criptografia baseada em senhas
public class S3P08_EncriptacaoBaseadaEmSenha {

    public static void main(String args[]) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, NoSuchProviderException, 
            InvalidKeySpecException, InvalidAlgorithmParameterException {

        Security.addProvider(new BouncyCastleProvider()); // provedor BC

        // configurações do PBE comuns para Ana e Bato
      char[] senha = "5enha!23".toCharArray();//8+ alfanum
      byte[] salt = U.x2b("1234567890ABCDEF");
      int iterationCount = 2048; // 1000+
      PBEKeySpec pbeks = new PBEKeySpec(senha, salt, iterationCount);
      SecretKeyFactory skf
              = SecretKeyFactory.getInstance("PBEWithSHA1And128BitAES-CBC-BC", "BC");
      Key sk = skf.generateSecret(pbeks);
      Cipher c = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");

      // Encriptação pela Ana
      c.init(Cipher.ENCRYPT_MODE, sk);
      byte[] textoclaroAna = "Testando o AES..".getBytes();
      byte[] criptograma = c.doFinal(textoclaroAna);

      // decriptação pelo Beto
      c.init(Cipher.DECRYPT_MODE, sk); //inicializa o AES para decriptacao
      byte[] textoclaroBeto = c.doFinal(criptograma); // Decriptando

        U.println("Chave gerada com: " + skf.getAlgorithm());
        U.println("Encriptado   com: " + c.getAlgorithm());
        U.println("Chave criptográfica: " + U.b2x(sk.getEncoded()));
        U.println("Init Vector gerado : " + U.b2x(c.getIV()));
        U.println("Criptograma (A-->B): " + U.b2x(criptograma));
        U.println("Texto claro do Beto: " + new String(textoclaroBeto));
    }
}
