/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec5;

// este faz a multiplicação rápida.

public class AES2 {

    public static final int KEYSIZE_128 = 4; // for 128-bit key, use 16, 16, and 4 below
    public static final int KEYSIZE_192 = 6; // for 192-bit key, use 16, 24 and 6 below
    public static final int KEYSIZE_256 = 8; // for 256-bit key, use 16, 32 and 8 below
    private final int Nb = 4; // words in a block, always 4 for now
    private int Nk; // key length in words
    private int Nr; // number of rounds, = Nk + 6
    private int wCount; // position in w for RoundKey (= 0 each encrypt)
    private AEStables tab; // all the tables needed for AES2
    private byte[] w; // the expanded key

    // AES2: constructor for class
    public AES2() {
        tab = new AEStables(); // class to give values of various functions
    }

    // Mainly expands key
    public void init(byte[] key, int NkIn) {
        Nk = NkIn; // words in a key, = 4, or 6, or 8
        Nr = Nk + 6; // corresponding number of rounds
        w = new byte[4 * Nb * (Nr + 1)]; // room for expanded key
        keyExpansion(key, w); // length of w depends on Nr
    }

// encrypt: actual AES2 encrytion
    public void encrypt(byte[] in, byte[] out) {
        wCount = 0; // count bytes in expanded key throughout encryption
        byte[][] state = new byte[4][Nb]; // the state array
        UtilAES.toState(state, in); // actual component-wise toState
        addRoundKey(state); // xor with expanded key
        for (int round = 1; round < Nr; round++) {
            //Util.printArray("Start round " + round + ":", state);
            subBytes(state); // S-box substitution
            shiftRows(state); // mix up rows
            mixColumns(state); // complicated mix of columns
            addRoundKey(state); // xor with expanded key
        }
        //Util.printArray("Start round " + Nr + ":", state);
        subBytes(state); // S-box substitution
        shiftRows(state); // mix up rows
        addRoundKey(state); // xor with expanded key
        UtilAES.fromState(out, state);
    }

    public void decrypt(byte[] in, byte[] out) {
        wCount = 4 * Nb * (Nr + 1); // count bytes during decryption
        byte[][] state = new byte[4][Nb]; // the state array
        UtilAES.toState(state, in); // actual component-wise toState
        invAddRoundKey(state); // xor with expanded key
        for (int round = Nr - 1; round >= 1; round--) {
            //Util.printArray("Start round " + (Nr - round) + ":", state);
            invShiftRows(state); // mix up rows
            invSubBytes(state); // inverse S-box substitution
            invAddRoundKey(state); // xor with expanded key
            invMixColumns(state); // complicated mix of columns
        }
        //Util.printArray("Start round " + Nr + ":", state);
        invShiftRows(state); // mix up rows
        invSubBytes(state); // inverse S-box substitution
        invAddRoundKey(state); // xor with expanded key
        UtilAES.fromState(out, state);
    }

// KeyExpansion: expand key, byte-oriented code, but tracks words
    private void keyExpansion(byte[] key, byte[] w) {
        byte[] temp = new byte[4];
// first just toState key to w
        int j = 0;
        while (j < 4 * Nk) {
            w[j] = key[j++];
        }
// here j == 4*Nk;
        int i;
        while (j < 4 * Nb * (Nr + 1)) {
            i = j / 4; // j is always multiple of 4 here
// handle everything word-at-a time, 4 bytes at a time
            for (int iTemp = 0; iTemp < 4; iTemp++) {
                temp[iTemp] = w[j - 4 + iTemp];
            }
            if (i % Nk == 0) {
                byte ttemp, tRcon;
                byte oldtemp0 = temp[0];
                for (int iTemp = 0; iTemp < 4; iTemp++) {
                    if (iTemp == 3) {
                        ttemp = oldtemp0;
                    } else {
                        ttemp = temp[iTemp + 1];
                    }
                    if (iTemp == 0) {
                        tRcon = tab.Rcon(i / Nk);
                    } else {
                        tRcon = 0;
                    }
                    temp[iTemp] = (byte) (tab.SBox(ttemp) ^ tRcon);
                }
            } else if (Nk > 6 && (i % Nk) == 4) {
                for (int iTemp = 0; iTemp < 4; iTemp++) {
                    temp[iTemp] = tab.SBox(temp[iTemp]);
                }
            }
            for (int iTemp = 0; iTemp < 4; iTemp++) {
                w[j + iTemp] = (byte) (w[j - 4 * Nk + iTemp] ^ temp[iTemp]);
            }
            j = j + 4;
        }
    }

// SubBytes: apply Sbox substitution to each byte of state
    private void subBytes(byte[][] state) {
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < Nb; col++) {
                state[row][col] = tab.SBox(state[row][col]);
            }
        }
    }

    private void invSubBytes(byte[][] state) {
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < Nb; col++) {
                state[row][col] = tab.invSBox(state[row][col]);
            }
        }
    }

// ShiftRows: simple circular shift of rows 1, 2, 3 by 1, 2, 3
    private void shiftRows(byte[][] state) {
        byte[] t = new byte[4];
        for (int r = 1; r < 4; r++) {
            for (int c = 0; c < Nb; c++) {
                t[c] = state[r][(c + r) % Nb];
            }
            for (int c = 0; c < Nb; c++) {
                state[r][c] = t[c];
            }
        }
    }

    private void invShiftRows(byte[][] state) {
        byte[] t = new byte[4];
        for (int r = 1; r < 4; r++) {
            for (int c = 0; c < Nb; c++) {
                t[(c + r) % Nb] = state[r][c];
            }
            for (int c = 0; c < Nb; c++) {
                state[r][c] = t[c];
            }
        }
    }

// MixColumns: complex and sophisticated mixing of columns
    private void mixColumns(byte[][] s) {
        int[] sp = new int[4];
        byte b02 = (byte) 0x02, b03 = (byte) 0x03;
        for (int c = 0; c < 4; c++) {
            sp[0] = tab.FFMulFast(b02, s[0][c]) ^ tab.FFMulFast(b03, s[1][c])
                    ^ s[2][c] ^ s[3][c];
            sp[1] = s[0][c] ^ tab.FFMulFast(b02, s[1][c])
                    ^ tab.FFMulFast(b03, s[2][c]) ^ s[3][c];
            sp[2] = s[0][c] ^ s[1][c]
                    ^ tab.FFMulFast(b02, s[2][c]) ^ tab.FFMulFast(b03, s[3][c]);
            sp[3] = tab.FFMulFast(b03, s[0][c]) ^ s[1][c]
                    ^ s[2][c] ^ tab.FFMulFast(b02, s[3][c]);
            for (int i = 0; i < 4; i++) {
                s[i][c] = (byte) (sp[i]);
            }
        }
    }

    private void invMixColumns(byte[][] s) {
        int[] sp = new int[4];
        byte b0b = (byte) 0x0b;
        byte b0d = (byte) 0x0d;
        byte b09 = (byte) 0x09;
        byte b0e = (byte) 0x0e;
        for (int c = 0; c < 4; c++) {
            sp[0] = tab.FFMulFast(b0e, s[0][c]) ^ tab.FFMulFast(b0b, s[1][c])
                    ^ tab.FFMulFast(b0d, s[2][c]) ^ tab.FFMulFast(b09, s[3][c]);
            sp[1] = tab.FFMulFast(b09, s[0][c]) ^ tab.FFMulFast(b0e, s[1][c])
                    ^ tab.FFMulFast(b0b, s[2][c]) ^ tab.FFMulFast(b0d, s[3][c]);
            sp[2] = tab.FFMulFast(b0d, s[0][c]) ^ tab.FFMulFast(b09, s[1][c])
                    ^ tab.FFMulFast(b0e, s[2][c]) ^ tab.FFMulFast(b0b, s[3][c]);
            sp[3] = tab.FFMulFast(b0b, s[0][c]) ^ tab.FFMulFast(b0d, s[1][c])
                    ^ tab.FFMulFast(b09, s[2][c]) ^ tab.FFMulFast(b0e, s[3][c]);
            for (int i = 0; i < 4; i++) {
                s[i][c] = (byte) (sp[i]);
            }
        }
    }

    // addRoundKey: xor a portion of expanded key with state
    private void addRoundKey(byte[][] state) {
        for (int c = 0; c < Nb; c++) {
            for (int r = 0; r < 4; r++) {
                state[r][c] = (byte) (state[r][c] ^ w[wCount++]);
            }
        }
    }
    // invAddRoundKey: same as AddRoundKey, but backwards

    private void invAddRoundKey(byte[][] state) {
        for (int c = Nb - 1; c >= 0; c--) {
            for (int r = 3; r >= 0; r--) {
                state[r][c] = (byte) (state[r][c] ^ w[--wCount]);
            }
        }
    }
}
