/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec5;

// este faz a multiplicação rápida.

public class AES3 {

    public static final int KEYSIZE_128 = 4; // for 128-bit key, use 16, 16, and 4 below
    public static final int KEYSIZE_192 = 6; // for 192-bit key, use 16, 24 and 6 below
    public static final int KEYSIZE_256 = 8; // for 256-bit key, use 16, 32 and 8 below
    private final int Nb = 4; // words in a block, always 4 for now
    private int Nk; // key length in words
    private int Nr; // number of rounds, = Nk + 6
    private int wCount; // position in w for RoundKey (= 0 each encrypt)
    private AEStables2 tab; // all the tables needed for AES2
    //private AEStables1 tab; // all the tables needed for AES2
    private byte[] w; // the expanded key

    // AES2: constructor for class
    public AES3() {
        tab = new AEStables2(); // class to give values of various functions
        //tab = new AEStables1(); // class to give values of various functions
    }
    
    

    // Mainly expands key
    public void init(byte[] key, int NkIn) {
        Nk = NkIn; // words in a key, = 4, or 6, or 8
        Nr = Nk + 6; // corresponding number of rounds
        w = new byte[4 * Nb * (Nr + 1)]; // room for expanded key
        keyExpansion(key, w); // length of w depends on Nr
    }

// encrypt: actual AES2 encrytion
    public void encrypt(byte[] in, byte[] out) {
        wCount = 0; // count bytes in expanded key throughout encryption
        byte[][] state = new byte[4][Nb]; // the state array
        UtilAES.toState(state, in); // actual component-wise toState
        addRoundKey(state); // xor with expanded key
        for (int round = 1; round < Nr; round++) {
            //Util.printArray("Start round " + round + ":", state);
            subBytes(state); // S-box substitution
            shiftRows(state); // mix up rows
            mixColumns(state); // complicated mix of columns
            addRoundKey(state); // xor with expanded key
        }
        //Util.printArray("Start round " + Nr + ":", state);
        subBytes(state); // S-box substitution
        shiftRows(state); // mix up rows
        addRoundKey(state); // xor with expanded key
        UtilAES.fromState(out, state);
    }

    public void decrypt(byte[] in, byte[] out) {
        wCount = 4 * Nb * (Nr + 1); // count bytes during decryption
        byte[][] state = new byte[4][Nb]; // the state array
        UtilAES.toState(state, in); // actual component-wise toState
        invAddRoundKey(state); // xor with expanded key
        for (int round = Nr - 1; round >= 1; round--) {
            //Util.printArray("Start round " + (Nr - round) + ":", state);
            invShiftRows(state); // mix up rows
            invSubBytes(state); // inverse S-box substitution
            invAddRoundKey(state); // xor with expanded key
            invMixColumns(state); // complicated mix of columns
        }
        //Util.printArray("Start round " + Nr + ":", state);
        invShiftRows(state); // mix up rows
        invSubBytes(state); // inverse S-box substitution
        invAddRoundKey(state); // xor with expanded key
        UtilAES.fromState(out, state);
    }

// KeyExpansion: expand key, byte-oriented code, but tracks words
    private void keyExpansion(byte[] key, byte[] w) {
        byte[] temp = new byte[4];
// first just toState key to w
        int j = 0;
        while (j < 4 * Nk) {
            w[j] = key[j++];
        }
// here j == 4*Nk;
        int i;
        while (j < 4 * Nb * (Nr + 1)) {
            i = j / 4; // j is always multiple of 4 here
// handle everything word-at-a time, 4 bytes at a time
            //for (int iTemp = 0; iTemp < 4; iTemp++) {
                temp[0] = w[j - 4 + 0];
                temp[1] = w[j - 4 + 1];
                temp[2] = w[j - 4 + 2];
                temp[3] = w[j - 4 + 3];
            //}
            if (i % Nk == 0) {
                byte ttemp, tRcon;
                byte oldtemp0 = temp[0];
                //for (int iTemp = 0; iTemp < 4; iTemp++) {
                    if (0 == 3) { ttemp = oldtemp0; }
                    else        { ttemp = temp[0 + 1]; }
                    if (0 == 0) { tRcon = tab.Rcon(i / Nk);}
                    else            { tRcon = 0; }
                    temp[0] = (byte) (tab.SBox(ttemp) ^ tRcon);
                    
                    if (1 == 3) { ttemp = oldtemp0; }
                    else        { ttemp = temp[1 + 1]; }
                    if (1 == 0) { tRcon = tab.Rcon(i / Nk);}
                    else            { tRcon = 0; }
                    temp[1] = (byte) (tab.SBox(ttemp) ^ tRcon);
                    
                    if (2 == 3) { ttemp = oldtemp0; }
                    else        { ttemp = temp[2 + 1]; }
                    if (2 == 0) { tRcon = tab.Rcon(i / Nk);}
                    else            { tRcon = 0; }
                    temp[2] = (byte) (tab.SBox(ttemp) ^ tRcon);
                    
                    if (3 == 3) { ttemp = oldtemp0; }
                    else        { ttemp = temp[3 + 1]; }
                    if (3 == 0) { tRcon = tab.Rcon(i / Nk);}
                    else            { tRcon = 0; }
                    temp[3] = (byte) (tab.SBox(ttemp) ^ tRcon);
                //}
            } else if (Nk > 6 && (i % Nk) == 4) {
                //for (int iTemp = 0; iTemp < 4; iTemp++) {
                    temp[0] = tab.SBox(temp[0]);
                    temp[1] = tab.SBox(temp[1]);
                    temp[2] = tab.SBox(temp[2]);
                    temp[3] = tab.SBox(temp[3]);
                //}
            }
//            for (int iTemp = 0; iTemp < 4; iTemp++) {
                w[j + 0] = (byte) (w[j - 4 * Nk + 0] ^ temp[0]);
                w[j + 1] = (byte) (w[j - 4 * Nk + 1] ^ temp[1]);
                w[j + 2] = (byte) (w[j - 4 * Nk + 2] ^ temp[2]);
                w[j + 3] = (byte) (w[j - 4 * Nk + 3] ^ temp[3]);
//            }
            j = j + 4;
        }
    }

// SubBytes: apply Sbox substitution to each byte of state
    private void subBytes(byte[][] state) {
        //for (int row = 0; row < 4; row++) {
            //for (int col = 0; col < Nb; col++) {
                state[0][0] = tab.SBox(state[0][0]);
                state[0][1] = tab.SBox(state[0][1]);
                state[0][2] = tab.SBox(state[0][2]);
                state[0][3] = tab.SBox(state[0][3]);
            //}
            //for (int col = 0; col < Nb; col++) {
                state[1][0] = tab.SBox(state[1][0]);
                state[1][1] = tab.SBox(state[1][1]);
                state[1][2] = tab.SBox(state[1][2]);
                state[1][3] = tab.SBox(state[1][3]);
            //}
            //for (int col = 0; col < Nb; col++) {
                state[2][0] = tab.SBox(state[2][0]);
                state[2][1] = tab.SBox(state[2][1]);
                state[2][2] = tab.SBox(state[2][2]);
                state[2][3] = tab.SBox(state[2][3]);
            //}
            //for (int col = 0; col < Nb; col++) {
                state[3][0] = tab.SBox(state[3][0]);
                state[3][1] = tab.SBox(state[3][1]);
                state[3][2] = tab.SBox(state[3][2]);
                state[3][3] = tab.SBox(state[3][3]);
            //}
        //}
    }

    private void invSubBytes(byte[][] state) {
        //for (int row = 0; row < 4; row++) {
            //for (int col = 0; col < Nb; col++) {
                state[0][0] = tab.invSBox(state[0][0]);
                state[0][1] = tab.invSBox(state[0][1]);
                state[0][2] = tab.invSBox(state[0][2]);
                state[0][3] = tab.invSBox(state[0][3]);
            //}
            //for (int col = 0; col < Nb; col++) {
                state[1][0] = tab.invSBox(state[1][0]);
                state[1][1] = tab.invSBox(state[1][1]);
                state[1][2] = tab.invSBox(state[1][2]);
                state[1][3] = tab.invSBox(state[1][3]);
            //}
            //for (int col = 0; col < Nb; col++) {
                state[2][0] = tab.invSBox(state[2][0]);
                state[2][1] = tab.invSBox(state[2][1]);
                state[2][2] = tab.invSBox(state[2][2]);
                state[2][3] = tab.invSBox(state[2][3]);
            //}
            //for (int col = 0; col < Nb; col++) {
                state[3][0] = tab.invSBox(state[3][0]);
                state[3][1] = tab.invSBox(state[3][1]);
                state[3][2] = tab.invSBox(state[3][2]);
                state[3][3] = tab.invSBox(state[3][3]);
            //}
        //}
    }

// ShiftRows: simple circular shift of rows 1, 2, 3 by 1, 2, 3
    private void shiftRows(byte[][] state) {
        byte[] t = new byte[4];
        //for (int r = 1; r < 4; r++) {
            //for (int c = 0; c < Nb; c++) {
                t[0] = state[1][(0 + 1) % 4];
                t[1] = state[1][(1 + 1) % 4];
                t[2] = state[1][(2 + 1) % 4];
                t[3] = state[1][(3 + 1) % 4];
            //}
            //for (int c = 0; c < Nb; c++) {
                state[1][0] = t[0];
                state[1][1] = t[1];
                state[1][2] = t[2];
                state[1][3] = t[3];
            //}
            //for (int c = 0; c < Nb; c++) {
                t[0] = state[2][(0 + 2) % 4];
                t[1] = state[2][(1 + 2) % 4];
                t[2] = state[2][(2 + 2) % 4];
                t[3] = state[2][(3 + 2) % 4];
            //}
            //for (int c = 0; c < Nb; c++) {
                state[2][0] = t[0];
                state[2][1] = t[1];
                state[2][2] = t[2];
                state[2][3] = t[3];
            //}
            //for (int c = 0; c < Nb; c++) {
                t[0] = state[3][(0 + 3) % 4];
                t[1] = state[3][(1 + 3) % 4];
                t[2] = state[3][(2 + 3) % 4];
                t[3] = state[3][(3 + 3) % 4];
            //}
            //for (int c = 0; c < Nb; c++) {
                state[3][0] = t[0];
                state[3][1] = t[1];
                state[3][2] = t[2];
                state[3][3] = t[3];
            //}
        //}
    }

    private void invShiftRows(byte[][] state) {
        byte[] t = new byte[4];
        //for (int r = 1; r < 4; r++) {
            //for (int c = 0; c < Nb; c++) {
                t[(0 + 1) % 4] = state[1][0];
                t[(1 + 1) % 4] = state[1][1];
                t[(2 + 1) % 4] = state[1][2];
                t[(3 + 1) % 4] = state[1][3];
            //}
            //for (int c = 0; c < Nb; c++) {
                state[1][0] = t[0];
                state[1][1] = t[1];
                state[1][2] = t[2];
                state[1][3] = t[3];
            //}
            //for (int c = 0; c < Nb; c++) {
                t[(0 + 2) % 4] = state[2][0];
                t[(1 + 2) % 4] = state[2][1];
                t[(2 + 2) % 4] = state[2][2];
                t[(3 + 2) % 4] = state[2][3];
            //}
            //for (int c = 0; c < Nb; c++) {
                state[2][0] = t[0];
                state[2][1] = t[1];
                state[2][2] = t[2];
                state[2][3] = t[3];
            //}
            //for (int c = 0; c < Nb; c++) {
                t[(0 + 3) % 4] = state[3][0];
                t[(1 + 3) % 4] = state[3][1];
                t[(2 + 3) % 4] = state[3][2];
                t[(3 + 3) % 4] = state[3][3];
            //}
            //for (int c = 0; c < Nb; c++) {
                state[3][0] = t[0];
                state[3][1] = t[1];
                state[3][2] = t[2];
                state[3][3] = t[3];
            //}
        //}
    }

// MixColumns: complex and sophisticated mixing of columns
    private void mixColumns(byte[][] s) {
        int[] sp = new int[4];
        byte b02 = (byte) 0x02, b03 = (byte) 0x03;
        //for (int c = 0; c < 4; c++) {
            sp[0] = tab.FFMulFast(b02, s[0][0]) ^ tab.FFMulFast(b03, s[1][0])
                    ^ s[2][0] ^ s[3][0];
            sp[1] = s[0][0] ^ tab.FFMulFast(b02, s[1][0])
                    ^ tab.FFMulFast(b03, s[2][0]) ^ s[3][0];
            sp[2] = s[0][0] ^ s[1][0]
                    ^ tab.FFMulFast(b02, s[2][0]) ^ tab.FFMulFast(b03, s[3][0]);
            sp[3] = tab.FFMulFast(b03, s[0][0]) ^ s[1][0]
                    ^ s[2][0] ^ tab.FFMulFast(b02, s[3][0]);
            //for (int i = 0; i < 4; i++) {
                s[0][0] = (byte) (sp[0]);
                s[1][0] = (byte) (sp[1]);
                s[2][0] = (byte) (sp[2]);
                s[3][0] = (byte) (sp[3]);
            //}
                
            sp[0] = tab.FFMulFast(b02, s[0][1]) ^ tab.FFMulFast(b03, s[1][1])
                    ^ s[2][1] ^ s[3][1];
            sp[1] = s[0][1] ^ tab.FFMulFast(b02, s[1][1])
                    ^ tab.FFMulFast(b03, s[2][1]) ^ s[3][1];
            sp[2] = s[0][1] ^ s[1][1]
                    ^ tab.FFMulFast(b02, s[2][1]) ^ tab.FFMulFast(b03, s[3][1]);
            sp[3] = tab.FFMulFast(b03, s[0][1]) ^ s[1][1]
                    ^ s[2][1] ^ tab.FFMulFast(b02, s[3][1]);
            //for (int i = 0; i < 4; i++) {
                s[0][1] = (byte) (sp[0]);
                s[1][1] = (byte) (sp[1]);
                s[2][1] = (byte) (sp[2]);
                s[3][1] = (byte) (sp[3]);
            //}
                
           sp[0] = tab.FFMulFast(b02, s[0][2]) ^ tab.FFMulFast(b03, s[1][2])
                    ^ s[2][2] ^ s[3][2];
            sp[1] = s[0][2] ^ tab.FFMulFast(b02, s[1][2])
                    ^ tab.FFMulFast(b03, s[2][2]) ^ s[3][2];
            sp[2] = s[0][2] ^ s[1][2]
                    ^ tab.FFMulFast(b02, s[2][2]) ^ tab.FFMulFast(b03, s[3][2]);
            sp[3] = tab.FFMulFast(b03, s[0][2]) ^ s[1][2]
                    ^ s[2][2] ^ tab.FFMulFast(b02, s[3][2]);
            //for (int i = 0; i < 4; i++) {
                s[0][2] = (byte) (sp[0]);
                s[1][2] = (byte) (sp[1]);
                s[2][2] = (byte) (sp[2]);
                s[3][2] = (byte) (sp[3]);
            //}
                
            sp[0] = tab.FFMulFast(b02, s[0][3]) ^ tab.FFMulFast(b03, s[1][3])
                    ^ s[2][3] ^ s[3][3];
            sp[1] = s[0][3] ^ tab.FFMulFast(b02, s[1][3])
                    ^ tab.FFMulFast(b03, s[2][3]) ^ s[3][3];
            sp[2] = s[0][3] ^ s[1][3]
                    ^ tab.FFMulFast(b02, s[2][3]) ^ tab.FFMulFast(b03, s[3][3]);
            sp[3] = tab.FFMulFast(b03, s[0][3]) ^ s[1][3]
                    ^ s[2][3] ^ tab.FFMulFast(b02, s[3][3]);
            //for (int i = 0; i < 4; i++) {
                s[0][3] = (byte) (sp[0]);
                s[1][3] = (byte) (sp[1]);
                s[2][3] = (byte) (sp[2]);
                s[3][3] = (byte) (sp[3]);
            //}
        //}
    }

    private void invMixColumns(byte[][] s) {
        int[] sp = new int[4];
        byte b0b = (byte) 0x0b;
        byte b0d = (byte) 0x0d;
        byte b09 = (byte) 0x09;
        byte b0e = (byte) 0x0e;
        //for (int c = 0; c < 4; c++) {
            sp[0] = tab.FFMulFast(b0e, s[0][0]) ^ tab.FFMulFast(b0b, s[1][0])
                    ^ tab.FFMulFast(b0d, s[2][0]) ^ tab.FFMulFast(b09, s[3][0]);
            sp[1] = tab.FFMulFast(b09, s[0][0]) ^ tab.FFMulFast(b0e, s[1][0])
                    ^ tab.FFMulFast(b0b, s[2][0]) ^ tab.FFMulFast(b0d, s[3][0]);
            sp[2] = tab.FFMulFast(b0d, s[0][0]) ^ tab.FFMulFast(b09, s[1][0])
                    ^ tab.FFMulFast(b0e, s[2][0]) ^ tab.FFMulFast(b0b, s[3][0]);
            sp[3] = tab.FFMulFast(b0b, s[0][0]) ^ tab.FFMulFast(b0d, s[1][0])
                    ^ tab.FFMulFast(b09, s[2][0]) ^ tab.FFMulFast(b0e, s[3][0]);
            //for (int i = 0; i < 4; i++) {
                s[0][0] = (byte) (sp[0]);
                s[1][0] = (byte) (sp[1]);
                s[2][0] = (byte) (sp[2]);
                s[3][0] = (byte) (sp[3]);
            //}
                
           sp[0] = tab.FFMulFast(b0e, s[0][1]) ^ tab.FFMulFast(b0b, s[1][1])
                    ^ tab.FFMulFast(b0d, s[2][1]) ^ tab.FFMulFast(b09, s[3][1]);
            sp[1] = tab.FFMulFast(b09, s[0][1]) ^ tab.FFMulFast(b0e, s[1][1])
                    ^ tab.FFMulFast(b0b, s[2][1]) ^ tab.FFMulFast(b0d, s[3][1]);
            sp[2] = tab.FFMulFast(b0d, s[0][1]) ^ tab.FFMulFast(b09, s[1][1])
                    ^ tab.FFMulFast(b0e, s[2][1]) ^ tab.FFMulFast(b0b, s[3][1]);
            sp[3] = tab.FFMulFast(b0b, s[0][1]) ^ tab.FFMulFast(b0d, s[1][1])
                    ^ tab.FFMulFast(b09, s[2][1]) ^ tab.FFMulFast(b0e, s[3][1]);
            //for (int i = 0; i < 4; i++) {
                s[0][1] = (byte) (sp[0]);
                s[1][1] = (byte) (sp[1]);
                s[2][1] = (byte) (sp[2]);
                s[3][1] = (byte) (sp[3]);
            //}
                
           sp[0] = tab.FFMulFast(b0e, s[0][2]) ^ tab.FFMulFast(b0b, s[1][2])
                    ^ tab.FFMulFast(b0d, s[2][2]) ^ tab.FFMulFast(b09, s[3][2]);
            sp[1] = tab.FFMulFast(b09, s[0][2]) ^ tab.FFMulFast(b0e, s[1][2])
                    ^ tab.FFMulFast(b0b, s[2][2]) ^ tab.FFMulFast(b0d, s[3][2]);
            sp[2] = tab.FFMulFast(b0d, s[0][2]) ^ tab.FFMulFast(b09, s[1][2])
                    ^ tab.FFMulFast(b0e, s[2][2]) ^ tab.FFMulFast(b0b, s[3][2]);
            sp[3] = tab.FFMulFast(b0b, s[0][2]) ^ tab.FFMulFast(b0d, s[1][2])
                    ^ tab.FFMulFast(b09, s[2][2]) ^ tab.FFMulFast(b0e, s[3][2]);
            //for (int i = 0; i < 4; i++) {
                s[0][2] = (byte) (sp[0]);
                s[1][2] = (byte) (sp[1]);
                s[2][2] = (byte) (sp[2]);
                s[3][2] = (byte) (sp[3]);
            //}
                
           sp[0] = tab.FFMulFast(b0e, s[0][3]) ^ tab.FFMulFast(b0b, s[1][3])
                    ^ tab.FFMulFast(b0d, s[2][3]) ^ tab.FFMulFast(b09, s[3][3]);
            sp[1] = tab.FFMulFast(b09, s[0][3]) ^ tab.FFMulFast(b0e, s[1][3])
                    ^ tab.FFMulFast(b0b, s[2][3]) ^ tab.FFMulFast(b0d, s[3][3]);
            sp[2] = tab.FFMulFast(b0d, s[0][3]) ^ tab.FFMulFast(b09, s[1][3])
                    ^ tab.FFMulFast(b0e, s[2][3]) ^ tab.FFMulFast(b0b, s[3][3]);
            sp[3] = tab.FFMulFast(b0b, s[0][3]) ^ tab.FFMulFast(b0d, s[1][3])
                    ^ tab.FFMulFast(b09, s[2][3]) ^ tab.FFMulFast(b0e, s[3][3]);
            //for (int i = 0; i < 4; i++) {
                s[0][3] = (byte) (sp[0]);
                s[1][3] = (byte) (sp[1]);
                s[2][3] = (byte) (sp[2]);
                s[3][3] = (byte) (sp[3]);
            //}
        //}
    }

    // addRoundKey: xor a portion of expanded key with state
    private void addRoundKey(byte[][] state) {
        //for (int c = 0; c < Nb; c++) {
            //for (int r = 0; r < 4; r++) {
                state[0][0] = (byte) (state[0][0] ^ w[wCount++]);
                state[1][0] = (byte) (state[1][0] ^ w[wCount++]);
                state[2][0] = (byte) (state[2][0] ^ w[wCount++]);
                state[3][0] = (byte) (state[3][0] ^ w[wCount++]);
                
                state[0][1] = (byte) (state[0][1] ^ w[wCount++]);
                state[1][1] = (byte) (state[1][1] ^ w[wCount++]);
                state[2][1] = (byte) (state[2][1] ^ w[wCount++]);
                state[3][1] = (byte) (state[3][1] ^ w[wCount++]);
                
                state[0][2] = (byte) (state[0][2] ^ w[wCount++]);
                state[1][2] = (byte) (state[1][2] ^ w[wCount++]);
                state[2][2] = (byte) (state[2][2] ^ w[wCount++]);
                state[3][2] = (byte) (state[3][2] ^ w[wCount++]);
                
                state[0][3] = (byte) (state[0][3] ^ w[wCount++]);
                state[1][3] = (byte) (state[1][3] ^ w[wCount++]);
                state[2][3] = (byte) (state[2][3] ^ w[wCount++]);
                state[3][3] = (byte) (state[3][3] ^ w[wCount++]);
            //}
        //}
    }
    // invAddRoundKey: same as AddRoundKey, but backwards

    private void invAddRoundKey(byte[][] state) {
        //for (int c = Nb - 1; c >= 0; c--) {
            //for (int r = 3; r >= 0; r--) {
                state[3][3] = (byte) (state[3][3] ^ w[--wCount]);
                state[2][3] = (byte) (state[2][3] ^ w[--wCount]);
                state[1][3] = (byte) (state[1][3] ^ w[--wCount]);
                state[0][3] = (byte) (state[0][3] ^ w[--wCount]);
                
                state[3][2] = (byte) (state[3][2] ^ w[--wCount]);
                state[2][2] = (byte) (state[2][2] ^ w[--wCount]);
                state[1][2] = (byte) (state[1][2] ^ w[--wCount]);
                state[0][2] = (byte) (state[0][2] ^ w[--wCount]);
                
                state[3][1] = (byte) (state[3][1] ^ w[--wCount]);
                state[2][1] = (byte) (state[2][1] ^ w[--wCount]);
                state[1][1] = (byte) (state[1][1] ^ w[--wCount]);
                state[0][1] = (byte) (state[0][1] ^ w[--wCount]);
                
                state[3][0] = (byte) (state[3][0] ^ w[--wCount]);
                state[2][0] = (byte) (state[2][0] ^ w[--wCount]);
                state[1][0] = (byte) (state[1][0] ^ w[--wCount]);
                state[0][0] = (byte) (state[0][0] ^ w[--wCount]);
            //}
        //}
    }
}
