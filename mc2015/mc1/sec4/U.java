/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec4;

/**
 *
 * @author Alexandre
 */
public class U {

  public static void println(String s) {
    System.out.println(s);
  }

  public static String b2s(byte[] ba) {
    return new String(ba);
  }

  public static byte[] x2b(String str) {
    if (str == null) {
      return null;
    } else if (str.length() < 2) {
      return null;
    } else {
      int len = str.length() / 2;
      byte[] buffer = new byte[len];
      for (int i = 0; i < len; i++) {
        buffer[i] = (byte) Integer.parseInt(
                str.substring(i * 2, i * 2 + 2), 16);
      }
      return buffer;
    }
  }

  public static String b2x(byte[] data) {
    if (data == null) {
      return null;
    } else {
      int len = data.length;
      String str = "";
      for (int i = 0; i < len; i++) {
        if ((data[i] & 0xFF) < 16) {
          str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
        } else {
          str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
        }
      }
      return str.toUpperCase();
    }
  }

  public static byte[] xor(byte[] a, byte[] b) {
    byte[] x = new byte[a.length];
    if (a.length <= b.length) {
      for (int i = 0; i < a.length; i++) {
        x[i] = (byte) (a[i] ^ b[i]);
      }
    }
    return x;
  }

  // incremento padrão para IV no modo CTR
  public static void stdIncIV(byte[] a, int mark) {
    int l = 0;
    if (mark >= 0 && mark < a.length) { l = mark;}
    for (int i = a.length - 1; i >= l; i--) {
      if ((a[i] & 0xFF) != 0xFF) {
        a[i] = (byte) (a[i] + 1);
        break;
      } else {
        a[i] = (byte) (0x00);
        continue;
      }
    }
  }

  public static void delay(int seg) {
    long x = seg * 1000;
    long t = System.currentTimeMillis();
    while ((System.currentTimeMillis() - t) < x) {
      ; // atraso de x segundos
    }
  }
  
}
