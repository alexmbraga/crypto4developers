/*
Introdução à Criptografia para Programadores
Evitando Maus Usos de Criptografia em Sistemas de Software
@author Alexandre Braga
*/
package mc2015.mc1.sec4;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

//1.4.1. Criptografia determinística simétrica
public class S4P01_CriptoDeterministicaECB {

  public static void main(String args[]) throws NoSuchAlgorithmException,
          NoSuchPaddingException, InvalidKeyException, BadPaddingException,
          IllegalBlockSizeException, NoSuchProviderException {

    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    byte[] textoClaroAna = ("Deterministica..").getBytes();
    KeyGenerator g = KeyGenerator.getInstance("AES", "BC");
    g.init(128);
    Key k = g.generateKey();
    String[] aes = {"AES","AES/ECB/NoPadding",
      "AES/ECB/PKCS5Padding", "AES/ECB/PKCS7Padding"};
    U.println("Texto claro   : " + new String(textoClaroAna));
    U.println("Chave AES     : " + U.b2x(k.getEncoded()));
    for (int a = 0; a < aes.length; a++) {
      Cipher enc = Cipher.getInstance(aes[a], "BC");
      enc.init(Cipher.ENCRYPT_MODE, k);
      Cipher dec = Cipher.getInstance(aes[a], "BC");
      dec.init(Cipher.DECRYPT_MODE, k);

      U.println("Encriptado com: " + enc.getAlgorithm());
      byte[][] criptograma = new byte[2][];
      for (int i = 0; i < 2; i++) {
        criptograma[i] = enc.doFinal(textoClaroAna);
        byte[] textoClaroBeto = dec.doFinal(criptograma[i]);
        U.println("Criptograma   : " + U.b2x(criptograma[i]));
        //U.println("Texto claro   : " + new String(textoClaroBeto));
      }
      //if (Arrays.equals(criptograma[0], criptograma[1])) {
      //  U.println("Iguais\n");
      //} else {
      //  U.println("Diferentes\n");
      //}
    }
  }
}
