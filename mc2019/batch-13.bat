echo OFF
cls
echo ########################################################################
echo Listagem 5.13. Assinaturas digitais e verificação de assinaturas com RSA
echo ########################################################################
echo.
echo ON

openssl dgst -sha512 -hex -sign alex.rsa gabarito.txt
@echo.
@pause

openssl dgst -sha512 -hex -sign alex.rsa gabarito.txt
@echo.
@pause


openssl dgst -sha512 -sign alex.rsa -out gabarito.sign gabarito.txt
@echo.
@pause

openssl rsa -in alex.rsa -pubout -out alex.pub
@echo.
@pause

openssl dgst -sha512 -verify alex.pub -signature gabarito.sign gabarito.txt
@echo.

:END
@echo DONE
