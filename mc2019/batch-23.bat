echo OFF
cls
echo #############################################
echo Listagem 5.23. Teste de parāmetros de DH/ECDH
echo #############################################
echo.
echo ON

IF "%1" == "" goto END 

openssl s_client -connect %1:443 -cipher ECDHE-ECDSA-AES256-SHA
@echo.
@pause

openssl s_client -connect %1 -port 443 -cipher kECDHE
@echo.
@pause

openssl s_client -connect %1 -port 443 -cipher kDHE
@echo.
@pause

:END
@echo DONE