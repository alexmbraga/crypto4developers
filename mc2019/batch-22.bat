echo OFF
cls
echo #############################################
echo Listagem 5.22. Teste de BEAST
echo #############################################
echo.
echo ON

IF "%1" == "" goto END 

openssl s_client -connect %1:443 -cipher "ALL:!RC4" -no_tls1_1 -no_tls1_2 -no_tls1_3
@echo.
@pause

openssl s_client -connect %1:443 -cipher "ALL:+RC4" -no_tls1_1 -no_tls1_2 -no_tls1_3
@echo.
@pause

:END
@echo DONE