echo OFF
cls
echo ###################################################################
echo Listagem 5.10. Encriptação com RSA-OAEP e chave de transporte SMIME
echo ###################################################################
echo.
echo ON

openssl genrsa -rand random.txt -aes-256-ctr -out par_chaves.rsa 2048
@echo.
@pause

openssl rsa -in par_chaves.rsa -pubout -out chave_pub.rsa
@echo.
@pause

openssl rsa -in par_chaves.rsa -out chave_priv.rsa
@echo.
@pause

openssl rsautl -encrypt -oaep -in gabarito.txt -pubin -inkey chave_pub.rsa -out gabarito.rsac
@echo.
@pause

type gabarito.rsac
@echo.
@pause

openssl rsautl -decrypt -oaep -in gabarito.rsac -inkey chave_priv.rsa -out gabarito6.txt
@echo.
@pause

openssl req -new -key par_chaves.rsa -x509 -out raiz.crt
@echo.
@pause

openssl smime -in gabarito.txt -encrypt -out gabarito.smime -aes-128-cbc raiz.crt
@echo.
@pause

type gabarito.smime
@echo.
@pause

openssl smime -decrypt -in gabarito.smime -inkey chave_priv.rsa
@echo.

:END
@echo DONE
