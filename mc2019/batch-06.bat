echo OFF
cls
echo #################################
echo Listagem 5.6. Encriptação com AES
echo #################################
echo.
echo ON

openssl enc -e -a -p -aes-256-ctr -pbkdf2 -in gabarito.txt -out gabarito.aes -k 1234
@echo.
@pause

type gabarito.aes
@echo.
@pause

openssl enc -d -a -p -aes-256-ctr -pbkdf2 -in gabarito.aes -out gabarito3.txt -k 1234
@echo.
@pause

type gabarito3.txt
@echo.
@pause

openssl enc -e -a -p -aes-256-ctr -in gabarito.txt -out gabarito.aes -K 6CB40CAEF6FDEDB31DBD908256F63276DB8FBC1E3430CEF18B6E0F5CD112D5F2 -iv E570548E2B1376147E3342F08082E29A
@echo.
@pause

type gabarito.aes
@echo.
@pause

openssl enc -d -a -p -aes-256-ctr -in gabarito.aes -out gabarito4.txt -K 6CB40CAEF6FDEDB31DBD908256F63276DB8FBC1E3430CEF18B6E0F5CD112D5F2 -iv E570548E2B1376147E3342F08082E29A
@echo.
@pause

type gabarito4.txt

:END
@echo.
@echo DONE