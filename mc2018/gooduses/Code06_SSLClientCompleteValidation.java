package mc2018.gooduses;

import mc2018._utils.CertUtils;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.PKIXRevocationChecker;
import java.security.cert.PolicyNode;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import javax.net.ssl.*;

/* https://docs.oracle.com/javase/8/docs/technotes/guides/security/jsse/JSSERefGuide.html
 * When using raw SSLSocket and SSLEngine classes, you should always check the
 * peer's credentials before sending any data. The SSLSocket and SSLEngine 
 * classes do not automatically verify that the host name in a URL matches the
 * host name in the peer's credentials. An application could be exploited with 
 * URL spoofing if the host name is not verified.
 */
public final class Code06_SSLClientCompleteValidation {

  public static void main(String[] args) throws Exception {
    SSLSocket s = null; boolean ok = true;
    try {
      SSLSocketFactory f =
              (SSLSocketFactory) SSLSocketFactory.getDefault();
      s = (SSLSocket) f.createSocket("www.google.com", 443);
      s.startHandshake();//all validations happen after handshake

      System.out.println("Session infos");
      SSLSession ss = s.getSession();
      System.out.println("Protocol: " + ss.getProtocol());
      System.out.println("Ciphersuite: " + ss.getCipherSuite());
      System.out.println("Host name: " + ss.getPeerHost());
      Principal peer = ss.getPeerPrincipal();
      System.out.println("SSL Peer Principal: " + peer);

      // Cert, date, and Host validation
      if (!peer.getName().contains("CN=" + ss.getPeerHost())) 
      {throw new CertificateException("Host and Principal mismatch");}

      Certificate[] peerCerts = ss.getPeerCertificates();
      if (peerCerts != null && peerCerts.length >= 2) {
        ((X509Certificate) peerCerts[0]).checkValidity();
        peerCerts[0].verify(peerCerts[1].getPublicKey());
      } else {throw new CertificateException("Unable to verify cert.");}
      
      // Cert Path validation
      // Step 1. Obtain CA root certs and the cert path to validate
      Certificate[] certs = ss.getPeerCertificates();
      X509Certificate[] x509certs = new X509Certificate[certs.length-1];
      for (int i = 0; i < certs.length-1; i++)
      { x509certs[i] = (X509Certificate) certs[i];}
      X509Certificate anchor = (X509Certificate) certs[certs.length-1];
      List l = Arrays.asList(x509certs);
      CertificateFactory cf = 
              CertificateFactory.getInstance("X.509","SUN");
      CertPath cp = cf.generateCertPath(l);

      // Step 2. Create a PKIXParameters with the trust anchors
      TrustAnchor ta = new TrustAnchor(anchor, null);
      PKIXParameters params = 
              new PKIXParameters(Collections.singleton(ta));

      // Step 3. Use a CertPathValidator to validate the certificate path
      CertPathValidator cpv = 
              CertPathValidator.getInstance("PKIX","SUN");
      PKIXRevocationChecker rc = 
              (PKIXRevocationChecker)cpv.getRevocationChecker();
      rc.setOptions(EnumSet.of(PKIXRevocationChecker.Option.SOFT_FAIL));
      rc.setOptions(EnumSet.of(PKIXRevocationChecker.Option.NO_FALLBACK));
      rc.setOptions(EnumSet.of(PKIXRevocationChecker.Option.ONLY_END_ENTITY));
      rc.setOptions(EnumSet.of(PKIXRevocationChecker.Option.PREFER_CRLS));
      params.addCertPathChecker(rc);
      
      // now it gets a valid CRL for Anchor
      X509CRL crl = CertUtils.getCRL(anchor);// supposed valid CRL
      List list = new ArrayList();list.add(crl);
      CertStoreParameters csp = new CollectionCertStoreParameters(list);
      CertStore store = CertStore.getInstance("Collection", csp);
      params.addCertStore(store);
      
      // validate certification path with specified params
      PKIXCertPathValidatorResult cpvr
              = (PKIXCertPathValidatorResult) cpv.validate(cp, params);
      PolicyNode policyTree = cpvr.getPolicyTree();
      PublicKey subjectPK = cpvr.getPublicKey();
    } catch (CertificateException|InvalidAlgorithmParameterException|
             NoSuchAlgorithmException|CertPathValidatorException e) 
    { System.out.println(e); ok = false; }
    if (ok){System.out.println();CertUtils.handleSocket(s);} 
    else {System.out.println("Something wrong in cert validation.");}
  }
}
