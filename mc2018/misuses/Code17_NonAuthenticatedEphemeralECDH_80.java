package mc2018.misuses;

import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import javax.crypto.*;

//Listagem 2.21. ECDH não autenticado e com chaves fracas.
public final class Code17_NonAuthenticatedEphemeralECDH_80 {

  public static void main(String argv[]) {
    try {
      KeyPairGenerator aKPG = KeyPairGenerator.getInstance("EC", "SunEC");
      aKPG.initialize(160); // this has only 112 bits of security
      KeyPair aKP = aKPG.generateKeyPair();

      KeyAgreement aKA = KeyAgreement.getInstance("ECDH", "SunEC");
      aKA.init(aKP.getPrivate());

      byte[] aPubKe = aKP.getPublic().getEncoded();

      KeyFactory bKF = KeyFactory.getInstance("EC", "SunEC");
      X509EncodedKeySpec x509ks = new X509EncodedKeySpec(aPubKe);
      PublicKey aPubK = bKF.generatePublic(x509ks);

      KeyPairGenerator bKPG = KeyPairGenerator.getInstance("EC", "SunEC");
      bKPG.initialize(160);
      KeyPair bKP = bKPG.generateKeyPair();
      KeyAgreement bKA = KeyAgreement.getInstance("ECDH", "SunEC");
      bKA.init(bKP.getPrivate());

      byte[] bPubKe = bKP.getPublic().getEncoded();

      KeyFactory aKF = KeyFactory.getInstance("EC", "SunEC");
      x509ks = new X509EncodedKeySpec(bPubKe);
      PublicKey bPubK = aKF.generatePublic(x509ks);
      aKA.doPhase(bPubK, true);
      byte[] aSecret = aKA.generateSecret();

      bKA.doPhase(aPubK, true);
      byte[] bSecret = bKA.generateSecret();

      if(!Arrays.equals(aSecret, bSecret))throw new Exception("Secrets differ");
    } catch (Exception e) {System.err.println("Error: " + e); System.exit(1);}
  }
}
