package mc2018.misuses;

import mc2018._utils.U;
import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import javax.crypto.*;

// Listagem 2.18. DH não autenticado e com parâmetros descartáveis ou efêmeros.
public final class Code14_NonAuthenticatedEphemeralDH_1024 {

  public static void main(String argv[]) {
    try {
      U.println("Alice generates DH keypair ...");
      KeyPairGenerator aKPG = KeyPairGenerator.getInstance("DH", "SunJCE");
      aKPG.initialize(1024);
      KeyPair aKP = aKPG.generateKeyPair();
      U.println("Alice creates and initializes her DH KeyAgreement object");
      KeyAgreement aKA = KeyAgreement.getInstance("DH", "SunJCE");
      aKA.init(aKP.getPrivate());

      U.println("Alice encodes her public key, and sends it over to Bob.");
      byte[] aPubK = aKP.getPublic().getEncoded();

      U.println("Turning to Bob");
      KeyFactory bKF = KeyFactory.getInstance("DH", "SunJCE");
      X509EncodedKeySpec x509ks = new X509EncodedKeySpec(aPubK);
      PublicKey apk = bKF.generatePublic(x509ks);

      U.println("Bob creates his own DH key pair");
      KeyPairGenerator bKPG = KeyPairGenerator.getInstance("DH", "SunJCE");
      bKPG.initialize(1024);
      KeyPair bKP = bKPG.generateKeyPair();
      U.println("Bob creates and initializes his DH KeyAgreement object");
      KeyAgreement bKA = KeyAgreement.getInstance("DH", "SunJCE");
      bKA.init(bKP.getPrivate());

      U.println("Bob encodes his public key");
      byte[] bPubK = bKP.getPublic().getEncoded();

      KeyFactory aKF = KeyFactory.getInstance("DH", "SunJCE");
      x509ks = new X509EncodedKeySpec(bPubK);
      PublicKey bobPubKey = aKF.generatePublic(x509ks);
      System.out.println("ALICE: Execute PHASE1 ...");
      aKA.doPhase(bobPubKey, true);
      byte[] aSecret = aKA.generateSecret();

      U.println("Bob uses Alice's public key for fisrt round");
      bKA.doPhase(apk, true);
      byte[] bSecret = bKA.generateSecret();

      if(!Arrays.equals(aSecret,bSecret)){throw new Exception("Secrets differ");} 
      else { U.println("Secrets are the same");}
    } catch (Exception e) {System.err.println("Error: " + e);System.exit(1); }
  }
}
