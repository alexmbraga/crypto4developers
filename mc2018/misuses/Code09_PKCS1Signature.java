package mc2018.misuses;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import mc2015.mc1.sec4.U;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Listagem 2.10. Assinatura digital RSA determinística.
 */
public final class Code09_PKCS1Signature {

  public static void main(String[] args) throws Exception {
    Security.addProvider(new BouncyCastleProvider()); // provedor BC
    KeyPairGenerator kg = KeyPairGenerator.getInstance("RSA", "BC");
    kg.initialize(2048, new SecureRandom());
    KeyPair kp = kg.generateKeyPair();
    
    Signature sig = Signature.getInstance("SHA512withRSA","SunRsaSign");
    byte[] m = "Testing RSA Signature PKCS1".getBytes();
    
    sig.initSign(kp.getPrivate(), SecureRandom.getInstanceStrong());
    sig.update(m); U.println("Signature: "+ U.b2x(sig.sign()));
    sig.initSign(kp.getPrivate(), SecureRandom.getInstanceStrong());
    sig.update(m); U.println("Signature: "+ U.b2x(sig.sign()));
    sig.initSign(kp.getPrivate(), SecureRandom.getInstanceStrong());
    sig.update(m); byte[] s = sig.sign();// generate a signature
    sig.update(m); U.println("Signature: "+ U.b2x(s));

    // verify a signature
    Signature verifier = Signature.getInstance("SHA512withRSA","BC");
    System.out.println("Algorithm: "+ sig.getAlgorithm());
    verifier.initVerify(kp.getPublic()); verifier.update(m);
    if (verifier.verify(s)) { U.println("Verification succeeded.");}
    else                    { U.println("Verification failed.");   }
  }
}
