package mc2018.misuses;

import mc2018._utils.U;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

// Listagem 2.11. RSA com PKCS#1.
public final class Code09a_UsePKCS1ForRSA {
  public static void main(String args[]) {
    try {
      Security.addProvider(new BouncyCastleProvider());// provedor BC
      byte[] ptAna = ("Randomized RSA").getBytes();
      KeyPairGenerator g = KeyPairGenerator.getInstance("RSA","BC");
      g.initialize(2048); KeyPair kp = g.generateKeyPair();

      Cipher e = Cipher.getInstance("RSA/None/PKCS1Padding","BC");
      e.init(Cipher.ENCRYPT_MODE, kp.getPublic());
      Cipher d = Cipher.getInstance("RSA/None/PKCS1Padding","BC");
      d.init(Cipher.DECRYPT_MODE, kp.getPrivate());

      U.println("Plaintext : " + new String(ptAna));
      U.println("Ciphertext: " + U.b2x(e.doFinal(ptAna)));
      U.println("Ciphertext: " + U.b2x(e.doFinal(ptAna)));
      U.println("Ciphertext: " + U.b2x(e.doFinal(ptAna)));
      U.println("Plaintext : " + U.b2x(d.doFinal(e.doFinal(ptAna))));
    } catch (NoSuchAlgorithmException | NoSuchPaddingException |
            InvalidKeyException | IllegalBlockSizeException |
            BadPaddingException | NoSuchProviderException e) 
    { System.out.println(e); }
  }
}
