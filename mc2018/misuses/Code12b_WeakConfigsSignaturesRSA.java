package mc2018.misuses;

import mc2018._utils.U;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Signature;

//Listagem 2.15. Três configurações inseguras de assinaturas digitais RSA.
public final class Code12b_WeakConfigsSignaturesRSA {

  public static void main(String[] args) throws Exception {
    // par de chaves de Ana e configurações do criptosistema
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "SunRsaSign");
    kpg.initialize(1024,SecureRandom.getInstanceStrong());
    String[] weakrsa = {"MD2withRSA", "MD5withRSA", "SHA1withRSA"};
    byte[] doc = U.cancaoDoExilio.getBytes();
    for (String rsa : weakrsa) {
      Signature signer = Signature.getInstance(rsa, "SunRsaSign");
      KeyPair kp1 = kpg.generateKeyPair();
      signer.initSign(kp1.getPrivate(), new SecureRandom());
      signer.update(doc);byte[] sign = signer.sign();
      
      Signature verifier = Signature.getInstance(rsa, "SunRsaSign");
      verifier.initVerify(kp1.getPublic()); verifier.update(doc);
      if (verifier.verify(sign)){U.println("Signature OK!");}
      else                      {U.println("Signature not OK!");}
    }
  }
}
