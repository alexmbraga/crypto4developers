package mc2018.misuses;

import mc2018._utils.U;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Signature;

//Listagem 2.14. ECDSA com chave de 112 bits mas segurança de 80 bits.
public final class Code12_SUN_80bits_ECDSA112wSHA1 {

    public static void main(String[] args) throws Exception {
        // par de chaves de Ana e configurações do criptosistema
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC","SunEC");
        kpg.initialize(112, SecureRandom.getInstanceStrong());
        Signature signer = Signature.getInstance("SHA1withECDSA","SunEC");
        KeyPair kp1 = kpg.generateKeyPair();

        signer.initSign(kp1.getPrivate(), new SecureRandom());
        byte[] doc = U.cancaoDoExilio.getBytes();
        signer.update(doc); byte[] sign = signer.sign();

        Signature verifier = Signature.getInstance("SHA1withECDSA","SunEC");
        verifier.initVerify(kp1.getPublic()); verifier.update(doc);
        
        if(!verifier.verify(sign)){System.out.println("Signature not OK!");}

        U.println("Curva "+ kpg.getAlgorithm());
        U.println("Algorithm: " + signer.getAlgorithm());
        U.println("Signature size: " + sign.length + " bytes");
        U.println("Signature: " + U.b2x(sign));
    }
}
